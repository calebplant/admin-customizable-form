import { LightningElement } from 'lwc';

export default class TesterCmp extends LightningElement {

    phoneValue;

    handlePhoneInputMask(event) {
        console.log('testerCmp :: handlePhoneInputMask');
        const d = event.detail.value
            .replace(/\D+/g, '')
            .match(/(\d{0,3})(\d{0,3})(\d{0,4})/);

        this.phoneValue = d[0] ? d[0] : undefined;
        event.target.value = !d[2] ? d[1] : `(${d[1]}) ${d[2]}` + (d[3] ? `-${d[3]}` : ``);
    }

    parseMatcherString(value) {
        
    }
}