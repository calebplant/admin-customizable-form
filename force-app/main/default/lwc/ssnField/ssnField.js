import { api, LightningElement } from 'lwc';

export default class SsnField extends LightningElement {

    @api maskValue;
    @api isRequired;
    @api
    get outputValue() {
        return this.ssnValue;
    }

    ssnValue;

    // Handlers
    handleSSNInputMask(event) {
        const d = event.target.value
            .replace(/\D+/g, '')
            .match(/(\d{0,3})(\d{0,2})(\d{0,4})/);
        this.ssnValue = d[0] ? d[0] : undefined;

        event.target.value = !d[2] ? d[1] : `${d[1]}-${d[2]}` + (d[3] ? `-${d[3]}` : ``);
    }

    handleBlur(event) {
        console.log('ssnField :: handleBlur');
        console.log(this.maskValue);

        if(this.maskValue) {
            const d = event.target.value
                .replace(/\D+/g, '')
                .match(/(\d{0,3})(\d{0,2})(\d{0,4})/);
            event.target.value = !d[2] ? this.repeatAsterix(d[1].length) : `${this.repeatAsterix(d[1].length)}-${this.repeatAsterix(d[2].length)}` + (d[3] ? `-${this.repeatAsterix(d[3].length)}` : ``);
        }
    }

    handleFocus(event) {
        if(event.target.value || this.ssnValue && this.maskValue) {
            const d = this.ssnValue
            .replace(/\D+/g, '')
            .match(/(\d{0,3})(\d{0,2})(\d{0,4})/);
        event.target.value = !d[2] ? d[1] : `${d[1]}-${d[2]}` + (d[3] ? `-${d[3]}` : ``);
        }
    }

    // Utils
    repeatAsterix(n) {
        return "*".repeat(n);
    }
}