import { LightningElement, api } from 'lwc';

export default class PhoneField extends LightningElement {

    @api selectedCountry;
    @api countryValues;
    @api isRequired;
    @api
    get outputValue() {
        return this.phoneValue;
    }

    phoneValue;
    phonePattern;
    internationalCode;
    replacementStr = '_p61_';

    get valuePlaceholder() {
        if(this.selectedCountry) {
            if(!this.phonePattern) {
                let foundCountry = JSON.parse(this.countryValues)[this.selectedCountry];
                this.phonePattern = foundCountry ? foundCountry.phonePattern : undefined;
                this.internationalCode = foundCountry.code ? `+${foundCountry.code} ` : '';
            }

            if(this.phonePattern) {
                let regexReplacementStr = this.replacementStr.replace(/_/g,'\\_');
                return this.phonePattern.replace(new RegExp(regexReplacementStr, 'g'), '#');
            }
        }
    }

    handlePhoneInputMask(event) {
        console.log('phoneField :: handlePhoneInputMask');
        if(!this.phonePattern) {
            let foundCountry = JSON.parse(this.countryValues)[this.selectedCountry];
            this.phonePattern = foundCountry ? foundCountry.phonePattern : undefined;
            this.internationalCode = foundCountry.code ? `+${foundCountry.code} ` : '';
        }
        console.log(event.target.value);
        let d = event.target.value;
        if(this.internationalCode) {
            d = d.replace(this.internationalCode, '');
        }
        d = d.replace(/\D+/g, '');
        this.phoneValue = d;
        
        if(this.phonePattern) {
            let mask = this.phonePattern;
            d.toString().split('').forEach(eachDigit => {
                console.log('eachdigit:' + eachDigit);
                mask = mask.replace(this.replacementStr, eachDigit);
            })
            let regexReplacementStr = this.replacementStr.replace(/_/g,'\\_');
            mask = mask.replace(new RegExp(regexReplacementStr, 'g'), '');
            mask = this.internationalCode + mask;
            console.log(mask);
            event.target.value = mask;
        }
    }

    handleClearPhone(event) {
        console.log('phoneField :: handleClearPhone');
        this.phoneValue = undefined;
        let phoneInput = this.template.querySelector('lightning-input');
        phoneInput.value = undefined;
    }
}