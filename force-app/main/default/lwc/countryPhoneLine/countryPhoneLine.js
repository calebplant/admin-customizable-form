import { api, LightningElement, wire } from 'lwc';
import fetchCountries from '@salesforce/apex/CountryProvinceUtils.getOrgCountries'

export default class CountryPhoneLine extends LightningElement {
    @api index;
    @api ph; // placeholder where numbers will later go

    @api getValues() {
        let phonePattern = this.phoneValue.replace(/\d/g, this.ph);
        return {
            country: this.countryValue,
            code: this.codeValue,
            phonePattern: phonePattern
        }
    }

    @api setPreviousValues(values) {
        console.log('countryPhoneLine :: setPreviousValues');
        // console.log(JSON.parse(JSON.stringify(values)));

        this.countryValue = values.country ? values.country : undefined;

        if(values.code) {
            this.codeValue = values.code;
            
            // const codeValueInput = this.template.querySelector("lightning-input[data-name='code']");
            // codeValueInput.value = this.codeValue;
        }

        if(values.phonePattern) {
            let regexReplacementStr = this.ph.replace(/_/g,'\\_');
            let mask = values.phonePattern.replace(new RegExp(regexReplacementStr, 'g'), '1');
            const phoneValueInput = this.template.querySelector("lightning-input[data-name='phone']");
            this.phoneValue = mask;
            phoneValueInput.value = mask;
        }

    }
    
    countryOptions;
    countryValue;
    codeValue;
    phoneValue;

    @wire(fetchCountries)
    getCountryOptions(response) {
        if(response.data) {
            console.log('Fetched countries....');
            this.countryOptions = response.data.map(eachCountry => {
                return {label: eachCountry, value: eachCountry};
            });
        }
        if(response.error) {
            console.log('Error fetching countries!');
            console.log(response.error);
        }
    }

    get showDeleteButton() {
        return this.index != 0; // dont show delete button on first line
    }

    // Handlers
    handleDelete(event) {
        this.dispatchEvent(new CustomEvent('deleteline', {detail: this.index}));
    }

    handlePhoneValidation(event) {
        let rawNumbers = event.target.value.replace(/\D+/g, '');
        if(rawNumbers.length == 0) {
            event.currentTarget.setCustomValidity('You must enter a value.');
            event.currentTarget.reportValidity();
        }
        else if((rawNumbers.length < 10 || rawNumbers.length > 15) && rawNumbers.length != 0) {
            event.currentTarget.setCustomValidity('Must contain between 10-15 numbers.');
            event.currentTarget.reportValidity();
        }
        else {
            event.currentTarget.setCustomValidity('');
            event.currentTarget.reportValidity();
            this.phoneValue = event.target.value;
        }
    }

    handleCodeInputMask(event) {
        console.log('countryPhoneLine :: handleCodeInputMask');
        console.log('val:');
        console.log(event.target.value);
        if(event.target.value) {
            let rawNumber = event.target.value
                .replace(/\D+/g, '');

            this.codeValue = rawNumber;
            event.target.value = rawNumber ? `+${rawNumber}` : undefined;
        }
    }

    handleCountryChange(event) {
        console.log('countryPhoneLine :: handleCodeInputMask');
        this.countryValue = event.target.value;
    }
}