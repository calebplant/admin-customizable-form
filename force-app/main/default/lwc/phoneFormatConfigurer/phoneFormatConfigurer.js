import { LightningElement, api } from "lwc";

export default class PhoneFormatConfigurer extends LightningElement {

    numOfCountries = 1;
    replacementStr = '_p61_';
    isRequiredWasSet = false;
    initialized = false;

    get numberOfCountries() {
        return [...Array(this.numOfCountries).keys()];
    }

    _inputVariables = [];

    @api
    get inputVariables() {
        return this._inputVariables;
    }
    set inputVariables(variables) {
        this._inputVariables = variables || [];
    }

    @api 
    builderContext;

    @api
    validate() {
        let validity = [];
        let countryLines = this.template.querySelectorAll('c-country-phone-line');
        let lineValues = {};

        countryLines.forEach(eachCountry => {
            let values = eachCountry.getValues();
            console.log('Values: ');
            console.log(JSON.parse(JSON.stringify(values)));
            lineValues[String(values.country)] = values;
        });

        console.log(JSON.parse(JSON.stringify(lineValues)));

        console.log('inputVariables:');
        console.log(JSON.parse(JSON.stringify(this._inputVariables)));
        console.log('builderContext: ');
        console.log(JSON.parse(JSON.stringify(this.builderContext)));

        this.handleValuesChange(JSON.stringify(lineValues));
        // if(!this.isRequiredWasSet) {

        // }
        // validity.push({
        //     key: 'Slider Range',
        //     errorString: 'The slider range is between 0 and 100.',
        // });
        return validity;
    }

    get isRequired() {
        const param = this.inputVariables.find(({name}) => name === 'isRequired');
        return param && param.value;
    }

    // get previousRequired() {
    //     const param = this.inputVariables.find(({name}) => name === 'isRequired');
    //     let result = '$GlobalConstant.True' ? true : false;
    //     console.log('result: ');
    //     console.log(result);
    //     return param.value == '$GlobalConstant.True' ? true : false;
    // }

    connectedCallback() {
        let previousValues = this.inputVariables.find(({name}) => name === 'countryValues');
        if(previousValues) {
            let values = JSON.parse(previousValues.value);
            this.numOfCountries = Object.keys(values).length;
        }
    }

    renderedCallback() {
        if(!this.initialized) {
            this.initialized = true;
            if(this.inputVariables.find(({name}) => name === 'isRequired')) {
                const isRequiredInput = this.template.querySelector("lightning-input[data-name='isRequired']");
                isRequiredInput.checked = true;
            }
            if(this.inputVariables.find(({name}) => name === 'selectedCountry')) {
                const selectedCountryField = this.template.querySelector("c-flow-combobox");
                let selectedCountry = this.inputVariables.find(({name}) => name === 'selectedCountry').value;
                selectedCountryField.value = selectedCountry;
                selectedCountryField.valueType = 'reference';
            }
            let previousValues = this.inputVariables.find(({name}) => name === 'countryValues');
            if(previousValues) {
                // console.log('previousValues: ');
                // console.log(JSON.parse(JSON.stringify(previousValues)));
                let values = JSON.parse(previousValues.value);
                // this.numOfCountries = Object.keys(values).length;
                let valuesList = [];
                for (let [key, value] of Object.entries(values)) {
                    valuesList.push(value)
                  }
                // console.log('valuesList: ');
                // console.log(valuesList);
                let countryLines = this.template.querySelectorAll('c-country-phone-line');

                let index=0;
                countryLines.forEach(eachLine => {
                    eachLine.setPreviousValues(valuesList[index]);
                    index++;
                });
            }
        }

    }

    // Handlers

    handleValuesChange(newValue) {
        const valueChangedEvent = new CustomEvent(
            'configuration_editor_input_value_changed', {
                 bubbles: true,
                 cancelable: false,
                 composed: true,
                 detail: {
                     name: 'countryValues',
                     newValue,
                     newValueDataType: 'String'
                 }
            }
        );
        this.dispatchEvent(valueChangedEvent);
    }
    
    handleSelectedCountryChanged(event) {
        console.log('event.detail');
        console.log(JSON.parse(JSON.stringify(event.detail)));

        if (event && event.detail) {
            let newDetail = {
                name: event.detail.id,
                newValue: `{!${event.detail.id}}`,
                newValueDataType: event.detail.newValueDataType
            }
            console.log('newDetail: ');
            console.log(newDetail);

            const valueChangedEvent = new CustomEvent(
                'configuration_editor_input_value_changed', {
                     bubbles: true,
                     cancelable: false,
                     composed: true,
                     detail: newDetail
                }
            );
            console.log('dispatching event');
            this.dispatchEvent(valueChangedEvent);
        }
    }

    handleIsRequiredChange(event) {
        console.log('PhoneFormatConfigurer :: handleIsRequiredChange');
        console.log(event.target.checked);
        if (event && event.detail) {
            const valueChangedEvent = new CustomEvent(
                'configuration_editor_input_value_changed', {
                     bubbles: true,
                     cancelable: false,
                     composed: true,
                     detail: {
                        name: 'isRequired',
                        newValue: event.target.checked,
                        newValueDataType: 'Boolean'
                    }
                }
            );
            console.log('dispatching event');
            this.dispatchEvent(valueChangedEvent);
            if(!this.isRequiredWasSet) {
                this.isRequiredWasSet = true;
            }
        }
    }

    handleAddCountry(event) {
        this.numOfCountries++;
    }

    handleRemoveCountry(event) {
        this.numOfCountries--;
    }
}