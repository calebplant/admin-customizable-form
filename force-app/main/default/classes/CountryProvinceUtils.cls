public with sharing class CountryProvinceUtils {
    
    @AuraEnabled(cacheable=true)
    public static List<String> getOrgCountries(){
        List<String> result = new List<String>();

        Schema.sObjectType objType = Contact.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        list<Schema.PicklistEntry> values = fieldMap.get('MailingCountryCode').getDescribe().getPickListValues();

        List<SelectOption> options = new List<SelectOption>();
        for (Schema.PicklistEntry v : values){
            result.add(v.getLabel());
            // options.add(new SelectOption(v.getLabel(), v.getLabel()));
        }
        // System.debug('---->'+options);
        return result;
    }
}
