# Admin-Customizable Form

## Overview

To achieve the requirements, we are able to use a Screen Flow. This gives the admin out-of-the-box customization for most fields. To cover advanced fields (for examaple, country-dependent phone number formatting) we can use a LWC custom property editor to present the admin options in a simplified manner.

## Demo (~90 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=F1pFJsdaLiM)

## Screenshot

![Form Builder](media/form-builder.png)

## Use

The admin is able to add countries whose number they want formated, and then enter an example phone number that the user's entered phone number will conform to as they type into the form. They are also able to set the dialing code.

![Form Use](media/form-use.png)

1. Country select

1. Example phone number

1. Dialing code